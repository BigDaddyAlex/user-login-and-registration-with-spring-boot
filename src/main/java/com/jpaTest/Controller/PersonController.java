package com.jpaTest.Controller;

import com.jpaTest.Model.Person;
import com.jpaTest.Model.Type;
import com.jpaTest.Service.PersonRepository;
import com.jpaTest.Service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class PersonController {

    @Autowired
    private PersonService personService;

    @GetMapping("/")
    public String Homepage(Model model){
        model.addAttribute("person",new Person());
        return "hw";
    }

    @PostMapping("/LogInCheck")
    public String LogInSubmit(@ModelAttribute("LogInForm") Person person, Model model) {
        model.addAttribute("person",person);
        if (personService.findPerson(person.getUsername()).isPresent()) return "LogInSuccess";
        else return "LogInFailure";
    }

    @GetMapping("/signup")
    public String SignUpPage(Model model){
        model.addAttribute("person",new Person());
        return "SignUp";
    }

    @PostMapping("/signup")
    public String SignUpSubmit(@ModelAttribute Person person, Model model){
        model.addAttribute("person",person);
        personService.addPerson(person.getUsername(),person.getPassword(), Type.Customer);
        return "LogInSuccess";
    }

    @GetMapping(value = "/create/{username}/{password}")
    @ResponseBody
    public void enlistPersonService(@PathVariable String username, @PathVariable String password){
        personService.addPerson(username,password, Type.Customer);
    }


}
