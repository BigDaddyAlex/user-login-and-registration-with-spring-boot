package com.jpaTest.Model;

import javax.persistence.*;

@Entity
public class Person {

    @Id
    String Username;
    @Column
    String Password;
    @Column
    Type type;

    public String getUsername() {
        return Username;
    }

    public String getPassword() {
        return Password;
    }

    public Type getType() {
        return type;
    }

    public Person(String username, String password, Type type) {
        this.Username = username;
        this.Password = password;
        this.type = type;
    }

    public Person() {
    }

    public void setUsername(String username) {
        Username = username;
    }

    public void setPassword(String password) {
        Password = password;
    }
}
