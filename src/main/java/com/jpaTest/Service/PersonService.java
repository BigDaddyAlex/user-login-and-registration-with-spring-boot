package com.jpaTest.Service;

import com.jpaTest.Model.Customer;
import com.jpaTest.Model.Person;
import com.jpaTest.Model.Type;
import com.jpaTest.Service.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PersonService {

    @Autowired
    private PersonRepository personRepository;

    public void addPerson(String username, String password, Type type){
        personRepository.save(new Person(username,password,type));
    }

    public Optional<Person> findPerson(String username) {
        return personRepository.findById(username);
    }
}
