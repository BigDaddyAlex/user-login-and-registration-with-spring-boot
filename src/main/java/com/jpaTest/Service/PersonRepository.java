package com.jpaTest.Service;

import com.jpaTest.Model.Customer;
import com.jpaTest.Model.Person;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonRepository extends CrudRepository<Person,String> {
}
